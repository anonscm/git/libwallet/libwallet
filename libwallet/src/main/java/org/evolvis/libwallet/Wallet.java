/**
 * 
 */
package org.evolvis.libwallet;

import org.evolvis.libwallet.impl.WalletException;

/**
 * This interface defines methods for accessing wallet-backends.
 * 
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface Wallet {
	
	/**
	 * Writes a password to a wallet
	 * 
	 * @param folder - The folder in which the password should be stored
	 * @param key - The key of the password to be stored 
	 * @param value - The password itself
	 * @throws WalletException - If the password could not be written to the backend
	 */
	public void writePassword(String folder, String key, String value) throws WalletException;
	
	/**
	 * Reads a password from a wallet
	 * 
	 * @param folder - The folder in which the password is stored
	 * @param key - The key of the password
	 * @return The password or null if the key does not exist
	 * @throws WalletException - If the wallet could not be accessed
	 */
	public String readPassword(String folder, String key) throws WalletException;
}
