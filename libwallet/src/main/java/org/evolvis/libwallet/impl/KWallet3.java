/**
 * 
 */
package org.evolvis.libwallet.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import org.evolvis.libwallet.Wallet;
import org.evolvis.libwallet.dcop.ObjectConverter;
import org.kde.DCOP.Client;
import org.kde.DCOP.Response;

/**
 * Implements Wallet for the Wallet-Backend in KDE3 called KWallet.
 * 
 * It uses DCOP for communication with the backend.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class KWallet3 implements Wallet {
	
	protected String appID;
	protected final static Logger logger = Logger.getLogger(KWallet3.class.getName());
	protected String walletName;
	protected Client client;
	protected int handle = -1;
	
	protected static int NETWORK_WALLET = 0;
	protected static int LOCAL_WALLET = 1;
	protected static int USER_DEFINED_WALLET = 2;

	public KWallet3(String appName) throws WalletException {
		this(appName, NETWORK_WALLET, null);
	}
	
	public KWallet3(String appName, boolean useLocalWallet) throws WalletException {
		this(appName, useLocalWallet ? LOCAL_WALLET : NETWORK_WALLET, null);
	}
	
	public KWallet3(String appName, String userDefinedWalletName) throws WalletException {
		this(appName, USER_DEFINED_WALLET, userDefinedWalletName);
	}
	
	protected KWallet3(String appName, int wallet, String userDefinedWalletName) throws WalletException {
		client = new Client();

		// register application to DCOP-server
		appID = client.registerAs(appName);
		

		if(appID == null)
			throw new WalletException("Could not register to DCOP server. Aborting.");	

		logger.fine("registered with appID: "+appID);


		if(client.isApplicationRegistered("kded"))
			logger.fine("Found kded. Proceeding...");
		else
			throw new WalletException("No kded found. Aborting.");

		if(wallet == NETWORK_WALLET)
			walletName = getNameOfNetworkWallet();
		else if(wallet == LOCAL_WALLET)
			walletName = getNameOfLocalWallet();
		else
			walletName = userDefinedWalletName;
		
		logger.fine("Using wallet \""+walletName+"\"");
		
		
		// try to open the wallet
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		DataOutputStream argsStream = new DataOutputStream(byteStream);
		try {
			ObjectConverter.write_QString(argsStream, walletName);
			ObjectConverter.write_int(argsStream, 1);
			
			
		} catch (IOException e) {
			throw new WalletException(e);
		}
		
		Response response = client.call("kded", "kwalletd", "open(QString, uint)", byteStream.toByteArray() , true);
		
		try {
			handle = ObjectConverter.readInt(new DataInputStream(new ByteArrayInputStream(response.returnData)));
		} catch (IOException e) {
			throw new WalletException(e);
		}
		if(handle < 1)
			throw new WalletException("Could not open wallet \""+walletName+"\"");
	}
	
	public String getNameOfNetworkWallet() throws WalletException {
		if(client == null)
			throw new WalletException("Connection to KWallet3 is not initialized properly");
		
		// determine the name of the network-wallet
		Response response = client.call("kded", "kwalletd", "networkWallet()", new byte[0], false);

		try {
			return ObjectConverter.read_QString(new DataInputStream(new ByteArrayInputStream(response.returnData)));
		} catch (IOException e) {
			throw new WalletException(e);
		}
	}
	
	public String getNameOfLocalWallet() throws WalletException {
		if(client == null)
			throw new WalletException("Connection to KWallet3 is not initialized properly");
		
		// determine the name of the local-wallet
		Response response = client.call("kded", "kwalletd", "localWallet()", new byte[0], false);

		try {
			return ObjectConverter.read_QString(new DataInputStream(new ByteArrayInputStream(response.returnData)));
		} catch (IOException e) {
			throw new WalletException(e);
		}
	}

	/**
	 * @see org.evolvis.libwallet.Wallet#readPassword(java.lang.String, java.lang.String)
	 */
	public String readPassword(String folder, String key) throws WalletException {
		logger.fine("Trying to read password from KWallet3");
		
		if(client == null || handle < 0 || appID == null || walletName == null)
			throw new WalletException("Connection to KWallet3 is not initialized properly");
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		DataOutputStream argsStream = new DataOutputStream(byteStream);
		try {
			ObjectConverter.write_int(argsStream, handle);
			ObjectConverter.write_QString(argsStream, folder);
			ObjectConverter.write_QString(argsStream, key);
			
		} catch (IOException e) {
			throw new WalletException(e);
		}
		
		Response response = client.call("kded", "kwalletd", "readPassword(int, QString, QString)", byteStream.toByteArray(), false);

		try {
			return ObjectConverter.read_QString(new DataInputStream(new ByteArrayInputStream(response.returnData)));
		} catch (IOException e) {
			throw new WalletException(e);
		}
	}

	/**
	 * @see org.evolvis.libwallet.Wallet#writePassword(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void writePassword(String folder, String key, String value)throws WalletException {
		logger.fine("Trying to write password to KWallet3");
		
		if(client == null || handle < 0 || appID == null || walletName == null)
			throw new WalletException("Connection to KWallet3 is not initialized properly");
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		DataOutputStream argsStream = new DataOutputStream(byteStream);
		try {
			ObjectConverter.write_int(argsStream, handle);
			ObjectConverter.write_QString(argsStream, folder);
			
		} catch (IOException e) {
			throw new WalletException(e);
		}
		
		Response response = client.call("kded", "kwalletd", "hasFolder(int, QString)", byteStream.toByteArray(), false);
		
		try {
			if(!ObjectConverter.read_bool(new DataInputStream(new ByteArrayInputStream(response.returnData)))) {
				
				logger.fine("folder \""+folder+"\" does not exist. creating it");
				// if folder does not exist yet, create it
				
				// recycle objects
				byteStream = new ByteArrayOutputStream();
				argsStream = new DataOutputStream(byteStream);
				try {
					ObjectConverter.write_int(argsStream, handle);
					ObjectConverter.write_QString(argsStream, folder);
					
				} catch (IOException e) {
					throw new WalletException(e);
				}
				
				response = client.call("kded", "kwalletd", "createFolder(int, QString)", byteStream.toByteArray(), false);
				
			}
		} catch (IOException e) {
			throw new WalletException(e);
		}
		
		// recycle objects
		byteStream = new ByteArrayOutputStream();
		argsStream = new DataOutputStream(byteStream);
		try {
			ObjectConverter.write_int(argsStream, handle);
			ObjectConverter.write_QString(argsStream, folder);
			ObjectConverter.write_QString(argsStream, key);
			ObjectConverter.write_QString(argsStream, value);
			
		} catch (IOException e) {
			throw new WalletException(e);
		}
		
		response = client.call("kded", "kwalletd", "writePassword(int, QString, QString, QString)", byteStream.toByteArray(), false);
	}

	public static boolean isAvailable() {
		
		try {
			// register application to DCOP-server
			Client client = new Client();
			client.attach();
	
	
			return client.isApplicationRegistered("kded");
		} catch(UnsatisfiedLinkError error) {
			logger.info("Could not connect to KWallet3. Reason: "+ error);
		} catch(NoClassDefFoundError error) {
			logger.info("Could not connect to KWallet3. Reason: "+ error);
		}
		
		return false;
	}
}
