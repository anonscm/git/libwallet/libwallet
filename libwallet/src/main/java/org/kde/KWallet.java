/**
 * 
 */
package org.kde;

import org.freedesktop.dbus.DBusInterface;

/**
 * A Java-Representation for the D-Bus-Object org.kde.KWallet
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface KWallet extends DBusInterface {

	public Integer open(String wallet, Long wld , String appID);
	public String networkWallet();
	public String localWallet();
	public String readPassword(Integer handle, String folder, String key, String appID);
	public Integer writePassword(Integer handle, String folder, String key, String value, String appID);
	public boolean hasFolder(Integer handle, String folder, String appID);
	public boolean createFolder(Integer handle, String folder, String appID);
}
