/**
 * 
 */
package org.evolvis.libwallet.impl;

import java.util.logging.Logger;

import org.evolvis.libwallet.Wallet;
import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.exceptions.DBusExecutionException;
import org.kde.KWallet;

/**
 * 
 * Implements Wallet for the Wallet-Backend in KDE4 called KWallet.
 * 
 * It uses D-BUS for communication with the backend.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class KWallet4 implements Wallet {

	protected KWallet kwallet;
	protected int handle = -1;
	protected String walletName;
	protected String appName;
	protected final static Logger logger = Logger.getLogger(KWallet4.class.getName());

	protected static int NETWORK_WALLET = 0;
	protected static int LOCAL_WALLET = 1;
	protected static int USER_DEFINED_WALLET = 2;

	public KWallet4(String appName) throws WalletException {
		this(appName, NETWORK_WALLET, null);
	}

	public KWallet4(String appName, boolean useLocalWallet) throws WalletException {
		this(appName, useLocalWallet ? LOCAL_WALLET : NETWORK_WALLET, null);
	}

	public KWallet4(String appName, String userDefinedWalletName) throws WalletException {
		this(appName, USER_DEFINED_WALLET, userDefinedWalletName);
	}

	protected KWallet4(String appName, int wallet, String userDefinedWalletName) throws WalletException {
		try {
			this.appName = appName;
			DBusConnection connection = DBusConnection.getConnection(DBusConnection.SESSION);

			logger.fine("registered with unique name \""+connection.getUniqueName()+"\"");

			// request a well-known bus-name
			connection.requestBusName("org.evolvis."+appName);

			// Get a reference to the kwallet-object
			kwallet = (KWallet)connection.getRemoteObject("org.kde.kded", "/modules/kwalletd", KWallet.class);

			if(wallet == NETWORK_WALLET)
				walletName = getNameOfNetworkWallet();
			else if(wallet == LOCAL_WALLET)
				walletName = getNameOfLocalWallet();
			else
				walletName = userDefinedWalletName;

			logger.fine("Using wallet \""+walletName+"\"");

			// Open the defined wallet
			handle = kwallet.open(walletName, Long.valueOf(1), appName);
			
			if(handle < 0)
				throw new WalletException("Access to Wallet \""+walletName+"\" was denied");

		} catch (DBusException e) {
			throw new WalletException("Could not init D-BUS connection.", e);
		}
	}

	/**
	 * @see org.evolvis.libwallet.Wallet#readPassword(java.lang.String, java.lang.String)
	 */
	public String readPassword(String folder, String key) throws WalletException {
		logger.fine("Trying to read password from KWallet4");

		if(kwallet == null || handle < 0 || appName == null)
			throw new WalletException("Connection to KWallet4 is not initialized properly");

		return kwallet.readPassword(handle, folder, key, appName);
	}

	/**
	 * @see org.evolvis.libwallet.Wallet#writePassword(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void writePassword(String folder, String key, String value) throws WalletException {
		logger.fine("Trying to read password from KWallet4");

		if(kwallet == null || handle < 0)
			throw new WalletException("Connection to KWallet4 is not initialized properly");


		if(!kwallet.hasFolder(handle, folder, appName)) {
			logger.fine("folder \""+folder+"\" does not exist. creating it");
			
			if(!kwallet.createFolder(handle, folder, appName))
				throw new WalletException("Could not create folder \""+folder+"\"");
		}

		Integer status = kwallet.writePassword(handle, folder, key, value, appName);
		
		if(status != 0)
			throw new WalletException("writePassword did not succeed. KWallet4 returned "+status);
	}

	public static boolean isAvailable() {
		try {
			// check if D-Bus works and kwallet-object is existant.
			KWallet kwallet = (KWallet)DBusConnection.getConnection(DBusConnection.SESSION).getRemoteObject("org.kde.kded", "/modules/kwalletd", KWallet.class);
			
			// Check if calls to this object work
			// TODO is there any method in dbus-java for just checking if a certain service is available?
			kwallet.networkWallet();
		} catch (DBusException e) {
			logger.info("Could not connect to KWallet4. Reason: "+e);
			return false;
		} catch(DBusExecutionException e) {
			logger.info("Could not connect to KWallet4. Reason: "+e);
			return false;
		} catch(UnsatisfiedLinkError e) {
			logger.info("Could not connect to KWallet4. Reason: "+e);
			return false;
		} catch(NoClassDefFoundError e) {
			logger.info("Could not connect to KWallet4. Reason: "+e);
			return false;
		}
		return true;
	}

	public String getNameOfNetworkWallet() throws WalletException {
		if(kwallet == null)
			throw new WalletException("Connection to KWallet4 is not initialized properly");

		// determine the name of the network-wallet
		return kwallet.networkWallet();
	}

	public String getNameOfLocalWallet() throws WalletException {
		if(kwallet == null)
			throw new WalletException("Connection to KWallet4 is not initialized properly");

		// determine the name of the network-wallet
		return kwallet.localWallet();

	}
}
