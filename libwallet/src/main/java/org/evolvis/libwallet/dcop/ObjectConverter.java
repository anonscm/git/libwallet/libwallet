/**
 * 
 */
package org.evolvis.libwallet.dcop;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Converts Java-Data-Types to DCOP-Data-Types and vice versa.
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ObjectConverter {

	/**
	 * Converts a QStringList into a String-array
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String[] read_QStringList(DataInputStream is) throws IOException
    {
        int n = is.readInt();
        String[] result = new String[n];
        for (int i=0; i<n; ++i)
            result[i] = read_QString(is);
        return result;
    }
	
	/**
	 * Converts a QString into a String
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String read_QString(DataInputStream is) throws IOException
    {
        int len = is.readInt();
        if (len == 0xffffffff)
            return new String();
        else
            {
                StringBuffer b = new StringBuffer();
                for (int i=0; i<len/2; ++i)
                    b.append(is.readChar());
                return b.toString();
            }
    }
	
	/**
	 * Converts a String into a QString
	 * 
	 * @param os
	 * @param val
	 * @throws IOException
	 */
	public static void write_QString(DataOutputStream os, String val) throws IOException
    {
        os.writeInt(val.length()*2);
        for (int i=0; i<val.length(); ++i)
            os.writeChar(val.charAt(i));
    }
	
	/**
	 * Converts a int to a Qt-Integer
	 * 
	 * @param os
	 * @param val
	 * @throws IOException
	 */
	public static void write_int(DataOutputStream os, int val) throws IOException
    {
        os.writeInt(val);
    }
	
	/**
	 * Converts a Qt-Integer into a int
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static int readInt(DataInputStream is) throws IOException
    {
		return is.readInt();
    }
	
	/**
	 * Converts a Qt-Boolean into a boolean
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
    public static boolean read_bool(DataInputStream is) throws IOException
    {
    	return is.readBoolean();
    }
}
