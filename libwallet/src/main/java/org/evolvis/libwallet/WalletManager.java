/**
 * 
 */
package org.evolvis.libwallet;

import org.evolvis.libwallet.impl.AppleKeychain;
import org.evolvis.libwallet.impl.KWallet3;
import org.evolvis.libwallet.impl.KWallet4;
import org.evolvis.libwallet.impl.WalletException;

/**
 * WalletManager provides a high-level API
 * for reading and writing sensitive data to a wallet-backend
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class WalletManager {
	
	protected static WalletManager instance;
	protected Wallet wallet;
	
	
	/**
	 * This class should not be instantiated.
	 * Use static method getInstance() instead. 
	 *
	 */
	protected WalletManager() {
		
	}
	
	/**
	 * Returns an instance of WalletManager
	 * 
	 * @return an instance of WalletManager
	 */
	public static WalletManager getInstance() {
		return getInstance(null);
	}
	
	/**
	 * Returns an instance of WalletManager
	 * 
	 * @param wallet - the wallet-backend to be used
	 * @return an instance of WalletManager
	 */
	public static WalletManager getInstance(Wallet wallet) {
		if(instance == null)
			instance = new WalletManager();
		
		instance.wallet = wallet;
		return instance;
	}
	
	/**
	 * This method is used internally for seaking a available wallet-backends
	 * 
	 * @param appName the app-name to be used for registering to the wallet-backend
	 * @throws WalletException if no supported wallet-backend was found 
	 */
	protected void checkForWallets(String appName) throws WalletException {
		// if a wallet was already defined, do not check
		if(wallet != null)
			return;
		
		if(KWallet4.isAvailable())
			wallet = new KWallet4(appName);
		else if(KWallet3.isAvailable())
			wallet = new KWallet3(appName);
		else if(AppleKeychain.isAvailable())
			wallet = new AppleKeychain();
		else
			throw new WalletException("No supported wallet-manager was found");
	}
	
	/**
	 * 
	 * Read a password from a wallet-backend
	 * 
	 * @param appName the app-name to be used for registering to the wallet-backend
	 * @param folder the folder in which the entry should be created
	 * @param key the key for the entry
	 * @return the value of the entry or null if not existant
	 * @throws WalletException
	 */
	public String readPassword(String appName, String folder, String key) throws WalletException {
		
		checkForWallets(appName);
		return wallet.readPassword(folder, key);
	}

	/**
	 * 
	 * Write a password to a wallet-backend
	 * 
	 * @param appName the app-name to be used for registering to the wallet-backend
	 * @param folder the folder in which the entry should be created
	 * @param key the key for the entry
	 * @param value the value of the entry
	 * @throws WalletException if no supported wallet-backend was found
	 */
	public void writePassword(String appName, String folder, String key, String value) throws WalletException {
		
		checkForWallets(appName);
		wallet.writePassword(folder, key, value);
	}
}
