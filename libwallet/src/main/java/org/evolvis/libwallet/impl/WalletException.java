/**
 * 
 */
package org.evolvis.libwallet.impl;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class WalletException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7674055142441222029L;

	/**
	 * 
	 */
	public WalletException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public WalletException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public WalletException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public WalletException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	
}
